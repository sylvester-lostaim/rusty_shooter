pub mod game_page {

    use crate::ncurses::{ erase, mvaddstr, mvaddch };
    use crate::values::axis;
    use crate::values::gameblock::single;
    use std::thread::spawn;

    mod moving_wall;
    use crate::game::game_page::moving_wall::moving_wall;
    mod enemy;
    use crate::game::game_page::enemy::enemy;
    mod player;
    use crate::game::game_page::player::player;

    pub fn paint_page() {
        // cleanup page
        erase();

        // title info
        mvaddstr(0, 0, "SCORE: 0");

        // draw gate
        // horizontal
        for x in 1..axis::X-1 { mvaddch(1, x, single::FENCE_X); }
        for x in 1..axis::X-1 { mvaddch(axis::Y-2, x, single::FENCE_X); }
        // vertical
        for y in 2..axis::Y-2 { mvaddch(y, 1, single::WALL); }
        for y in 2..axis::Y-2 { mvaddch(y, axis::X-2, single::WALL); }
        for y in 2..axis::Y-2 { mvaddch(y, axis::X-10, single::FENCE_Y); }
        
        spawn(|| { moving_wall(); });
        spawn(|| { enemy(); });
        player();
  }
}
