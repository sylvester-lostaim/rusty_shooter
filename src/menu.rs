pub mod menu_page {

    use crate::ncurses::{ addstr, getch, refresh };
    use crate::values::keypress;
    use crate::values::axis;

    pub fn paint_page() -> bool {
        let instruction = [ "Rusty Shooter

TERMINAL SIZE / PAINTING AREA
x-axis                     ",
&axis::X.to_string(),
"
y-axis                     ", 
&axis::Y.to_string(),
"

ACTION                     KEY
Move to top                Arrow Up
Move to bottom             Arrow Down
Move to left               Arrow Left
Move to right              Arrow Right
Quit                       q

Press \"Enter\" to start ..."
        ].concat();
        addstr(&instruction);
        refresh();
        return user_response();
  }

    fn user_response() -> bool {
        loop {
            let response = getch();
            if response == keypress::KEY_ENTER {
                return true;
            } else if response == keypress::KEY_Q {
                return false;
            }
        }
    }
}
