use rand::Rng;
use std::thread::sleep;
use std::time::Duration;
use crate::ncurses::{ mvaddch, mvinch };
use crate::values::gameblock::single;
use crate::values::speed_ms;
use crate::values::axis;

pub fn enemy() {
    initialize();
}

fn initialize() {
    let mut enemies: Vec<[i32; 2]> = Vec::new();

    // draw 7 enemies
    // give no enemy spawn safezone 
    // for player when initialize
    for _ in 0..7 { enemies.push(draw_enemy(&3)); }

    loop {
        sleep(Duration::from_millis(speed_ms::ENEMY));
        enemies = enemy_movement(enemies);
    }
}

fn draw_enemy(safezone: &i32) -> [i32; 2]{
    let mut x = 0;
    let mut y = 0;

    // randomly spawn ENEMY gameblock in enemy area
    while mvinch(y, x) != single::EMPTY {
      x = rand::thread_rng().gen_range(axis::X-9..axis::X-3);
      y = rand::thread_rng().gen_range(2..axis::Y-3-safezone);
    }

    // place enemy
    mvaddch(y, x, single::ENEMY);
    return [y, x];
}

fn enemy_movement(mut enemies: Vec<[i32; 2]>) -> Vec<[i32; 2]> {
    for i in 0..enemies.len() as usize {
        // try five times for random action
        // there is a condition that an enemy with obstacle surrounded
        // but random action outcome is moving, not shooting
        let mut counter = 0;
        while counter < 5 {
            // 50% shooting, 50% moving
            let action = rand::thread_rng().gen_range(0..2);

            // shooting
            if action == 0 {
                break;

            // moving
            // UP = 0, DOWN = 1, LEFT = 2, RIGHT = 3
            // remove enemy, update coordinate, place enemy
            } else {
                let movement = rand::thread_rng().gen_range(0..4);
                // move UP
                if movement == 0 {
                    if mvinch(enemies[i][0] - 1, enemies[i][1]) == single::EMPTY {
                        mvaddch(enemies[i][0], enemies[i][1], single::EMPTY);
                        enemies[i][0] = enemies[i][0] - 1;
                        mvaddch(enemies[i][0], enemies[i][1], single::ENEMY);

                    } else {
                        continue;
                    }

                // move DOWN
                } else if movement == 1 {
                    if mvinch(enemies[i][0] + 1, enemies[i][1]) == single::EMPTY {
                        mvaddch(enemies[i][0], enemies[i][1], single::EMPTY);
                        enemies[i][0] = enemies[i][0] + 1;
                        mvaddch(enemies[i][0], enemies[i][1], single::ENEMY);

                    } else {
                        continue;
                    }

                // move LEFT
                } else if movement == 2 {
                    if mvinch(enemies[i][0], enemies[i][1] - 1) == single::EMPTY {
                        mvaddch(enemies[i][0], enemies[i][1], single::EMPTY);
                        enemies[i][1] = enemies[i][1] - 1;
                        mvaddch(enemies[i][0], enemies[i][1], single::ENEMY);

                    } else {
                        continue;
                    }

                // move RIGHT
                } else if movement == 3 {
                    if mvinch(enemies[i][0], enemies[i][1] + 1) == single::EMPTY {
                        mvaddch(enemies[i][0], enemies[i][1], single::EMPTY);
                        enemies[i][1] = enemies[i][1] + 1;
                        mvaddch(enemies[i][0], enemies[i][1], single::ENEMY);

                    } else {
                        continue;
                    }
                }
            }
            counter = counter + 1;
        }
    }
    return enemies;
}
