use std::process::exit;
use crate::values::axis;
use crate::values::keypress;
use crate::values::gameblock::single;
use crate::ncurses::{ mvinch, mvaddch, getch, timeout, endwin };

pub fn player() {
    initialize();
}

fn initialize() {
    let mut player = [axis::Y-4, axis::X/3];
    mvaddch(player[0], player[1], single::PLAYER);

    timeout(1);     // accept user response every 1ms
    loop {
        let response = getch();
        player = bound_by_wall(player);
        player = user_response(response, player);
    }
}

fn user_response(response: i32, mut player: [i32; 2]) -> [i32; 2] {
    if response == keypress::KEY_Q {
        endwin();
        exit(0);

    } else if response == keypress::KEY_UP {
        if mvinch(player[0] - 1, player[1]) == single::EMPTY {
            mvaddch(player[0], player[1], single::EMPTY);
            player[0] = player[0] - 1;
            mvaddch(player[0], player[1], single::PLAYER);
        }

    } else if response == keypress::KEY_DOWN {
        if mvinch(player[0] + 1, player[1]) == single::EMPTY {
            mvaddch(player[0], player[1], single::EMPTY);
            player[0] = player[0] + 1;
            mvaddch(player[0], player[1], single::PLAYER);
        }

    } else if response == keypress::KEY_LEFT {
        if mvinch(player[0], player[1] - 1) == single::EMPTY {
            mvaddch(player[0], player[1], single::EMPTY);
            player[1] = player[1] - 1;
            mvaddch(player[0], player[1], single::PLAYER);
        }

    } else if response == keypress::KEY_RIGHT {
        if mvinch(player[0], player[1] + 1) == single::EMPTY {
            mvaddch(player[0], player[1], single::EMPTY);
            player[1] = player[1] + 1;
            mvaddch(player[0], player[1], single::PLAYER);
        }
    }
    
    return player;
}

// check whether player and wall in same coordinates (y,x)
// auto-bound to left by wall
//
// if axis-x is 1, player is bound by moving wall into fixed wall (non-moving)
// outcome condition is death
fn bound_by_wall(mut player: [i32; 2]) -> [i32; 2] {
    while mvinch(player[0], player[1]) == single::WALL {
        // bound player to left
        player[1] = player[1] - 1;

        // player had been bound into fixed wall
        if player[1] < 2 {
            end_game(false);        // bad end game
        }
    }

    // place player after bound coordinate
    mvaddch(player[0], player[1], single::PLAYER);
    return player;
}

fn end_game(state: bool) {
    if state {
        println!("{}", "OK")
    } else {
        println!("{}", "NOT OK")
        //mvaddstr();
    }
    endwin();
    exit(0);
}
