use crate::values::gameblock::single;
use crate::values::gameblock::multi;
use crate::values::speed_ms;
use crate::values::axis;
use crate::ncurses::{ mvinch, mvaddch };
use rand::Rng;
use std::thread::sleep;
use std::time::Duration;

// draw from top to bottom, y-axis
// give space between multi-blocks with SEPARATOR_?
// draw 5 units in vertical
// draw 6 units in horizontal
const SEPARATOR_X: i32 = 4;
const SEPARATOR_Y: i32 = 3;
const UNIT_Y: i32 = 5;
const UNIT_X: i32 = 7;

pub fn moving_wall() {
    handler();
}

fn handler() {
    // starts game
    initialize();
    
    // every 2 moves generate new column of walls
    let mut count = 0;

    // move the wall
    loop {
        // add new wall
        if count == 6 {
            let mut mover = [2 + SEPARATOR_Y, axis::X - 12];

            // draw for new last column on right
            for _ in 0..UNIT_Y {
                draw_multiblock(&mover);
                mover[0] = mover[0] + SEPARATOR_Y + multi::BOTTOM_LEFT.len() as i32;
            }
            count = 0;  //recount
        }

        // wall moving speed
        sleep(Duration::from_millis(speed_ms::WALL));
        wall_movement();

        // move counter
        count = count + 1;
    }
}

fn initialize() {
    // always draw from left top
    // mover is an indicator for drawing
    // starting from first coordinate inside gate (2,2) / (y, x)
    let mut mover: [i32; 2] = [2 + SEPARATOR_Y, 2 + SEPARATOR_X];

    for _ in 0..UNIT_X {
        // draw for y-axis
        for _ in 0..UNIT_Y {
            draw_multiblock(&mover);
            mover[0] = mover[0] + SEPARATOR_Y + multi::BOTTOM_LEFT.len() as i32;
        }

        // re-initialize y-axis
        // move x-axis to right
        mover[0] = 2 + SEPARATOR_Y;
        mover[1] = mover[1] + SEPARATOR_X + multi::BOTTOM_LEFT[0].len() as i32;
    }
}

fn draw_multiblock(mover: &[i32; 2]) {
    let multiblock = random_multiblock();
    for y in 0..multiblock.len() {
        for x in 0..multiblock[y].len() {
            if multiblock[y][x] {
                mvaddch(
                    mover[0] + y as i32,
                    mover[1] + x as i32,
                    single::WALL
                );
            } else {
                mvaddch(
                    mover[0] + y as i32,
                    mover[1] + x as i32,
                    single::EMPTY
                );
            }
        }
    }
}

fn random_multiblock() -> [[bool; 2]; 2] {
    // 80% for single block as wall
    // 20% for 3-blocks as wall
    let block_type = rand::thread_rng().gen_range(0..5);

    // 3-blocks
    if block_type == 0 {
        // there are 4 type of 3-blocks wall
        // each 25% to appear
        let target_block = rand::thread_rng().gen_range(0..4);
        if target_block == 0 {
            return multi::TOP_LEFT;
        } else if target_block == 1 {
            return multi::TOP_RIGHT;
        } else if target_block == 2 {
            return multi::BOTTOM_LEFT;
        } else {
            return multi::BOTTOM_RIGHT;
        }

    // 1-block
    } else {
        // there are 2 type of 1-block wall
        // each 50% to appear
        let target_block = rand::thread_rng().gen_range(0..2);
        if target_block == 0 {
            return multi::SINGLE_TOP;
        } else {
            return multi::SINGLE_BOTTOM;
        }
    }
}

fn wall_movement() {
        // redraw from left top
        // starting point to redraw (y, x)
        let mut redraw = [4, 2];
        let last_redraw = [axis::Y-5, axis::X-10];

        // draw column by column
        // full draw from left to right
        while redraw[1] < last_redraw[1] {
            while redraw[0] < last_redraw[0] {
                if mvinch(redraw[0], redraw[1]) == single::WALL {
                    mvaddch(redraw[0], redraw[1]-1, single::WALL);
                    mvaddch(redraw[0], redraw[1], single::EMPTY);
                }
                redraw[0] = redraw[0] + 1;
            }
            //re-initialize y-axis
            redraw[0] = 4;
            redraw[1] = redraw[1] + 1;
        }
}
