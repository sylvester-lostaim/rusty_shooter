extern crate ncurses;
use ncurses::*;

mod values;
use crate::values::axis;
mod menu;
use crate::menu::menu_page;
mod game;
use crate::game::game_page;

fn main() {
    initscr();                                        // initialize ncurse
    resizeterm(axis::Y, axis::X + 30);                // fixed painting area, +30 for menu
    curs_set(CURSOR_VISIBILITY::CURSOR_INVISIBLE);    // hide cursor
    noecho();                                         // disable keypress feedback
    raw();                                            // dont process signal

    // launch menu page
    if menu_page::paint_page() {
        // if true, user starts game
        game_page::paint_page()
    } 
    endwin();
}
