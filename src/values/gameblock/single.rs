pub const EMPTY: u32 = 32;             // whitespace
pub const WALL: u32 = 35;              // #
//pub const BULLET: u32 = 45;            // -
//pub const EXPLODE: u32 = 48;           // 0, if two bullets crashes
//
pub const ENEMY: u32 = 60;             // <
pub const PLAYER: u32 = 62;            // >
//pub const DEAD: u32 = 88;              // X

pub const FENCE_X: u32 = 61;           // =
pub const FENCE_Y: u32 = 124;          // |
//pub const FENCE_BULLET: u32 = 43;      // +
