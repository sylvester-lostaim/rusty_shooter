pub const TOP_LEFT: [[bool; 2]; 2] = [
    [true, true],                    // ##
    [true, false]                    // #-
];

pub const TOP_RIGHT: [[bool; 2]; 2] = [
    [true, true],                    // ##
    [false, true]                    // -#
];

pub const BOTTOM_LEFT: [[bool; 2]; 2] = [
    [true, false],                   // #-
    [true, true]                     // ##
];

pub const BOTTOM_RIGHT: [[bool; 2]; 2] = [
    [false, true],                   // -#
    [true, true]                     // ##
];

pub const SINGLE_TOP: [[bool; 2]; 2] = [
    [true, false],                   // #-
    [false, false]                   // --
];

pub const SINGLE_BOTTOM: [[bool; 2]; 2] = [
    [false, false],                  // --
    [false, true]                    // -#
];
