// SIMPLIFIED DRAWING 
// (early draft, may be different to actual outcome)
//
//        x-axis: 30
//       (2 empty ends, 22 player areas, 5 enemy areas, 2 walls, 1 fence)
// 
//     ==================
//     #         |      #
//     #  ##  ## |      #
//     #   #  #  |- <   #
//     #  >      |      #
//     #   #   # |   X  #     
//     #  ##  ## |      #     y-axis = 17 (2 empty ends)
//     #         +    < #
//     #   #   # |  <   #
//     #  ##  ## |      #
//     #         |      #
//     ==================
//
//     ^         ^      ^
//     |<------->|<---->|
//     |  ^      |  ^   |
//     |  |      |  |   +--------------------- fixed wall
//     |  |      |  +-------------------- enemy auto-spawning area
//     |  |      +---------------- player's dying border, moving wall generator
//     |  +-------------- player's area, wall keeps moving to left
//     +-----------fixed wall
//
//
// RANDOMLY GENERATED MOVING WALL TYPE ('-' indicates EMPTY)
// - block is calculated and drawn from left top coordinate (4 coordinates for each block)
//
// ##-     ##-     #--     -#-     #--     ---
// -#-     #--     ##-     ##-     ---     -#-
//
// <          20%            >     <   80%   >

pub mod axis;
pub mod gameblock;
pub mod keypress;
pub mod speed_ms;
