# A Rusty Shooter
- Just for fun where written in rust.
- A ncurses terminal-based shooting game.
- Just testing on multi-threading.
- Currently, there is no bullet but just moveable.

# Requirement
- Only tested on Linux. (maybe just works on Windows out-of-the-box)
- Low level ncurses is enough.

# Installation
- Ensure you have rust cargo installed.
- Clone the project, change into the directory and compile it.
```
cargo run          # it will auto-build if not yet built
```

# Interface
## Game Block
| Symbol | Represent |
| ------ | ------ |
| > | player |
| < | enemy |
| # | wall |

## Dummy Test
![test](images/test.gif)

## Unexpected Drawing
- Player is drawn in unexpected coordinate.
    - top right
    - bottom left
    - center left
- Happens after feature applied, *wall will bound back player*.
- There are 3 running threads for ncurses drawing.
- Maybe my bad handling in multi-threading?
- or what's going on in memory?
- ncurses is not thread safe, should I handle with mutex on my own?

![unexpected](images/unexpected.gif)

# Hierarchy
```
src
├── game
│   └── game_page
│       ├── enemy.rs
│       ├── moving_wall.rs
│       └── player.rs
├── game.rs
├── main.rs
├── menu.rs
├── values
│   ├── axis.rs
│   ├── gameblock
│   │   ├── multi.rs
│   │   └── single.rs
│   ├── gameblock.rs
│   ├── keypress.rs
│   └── speed_ms.rs
└── values.rs
```
